#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "wave.h"
#include "io.h"

//Render Tone

/* Checks args
 * Returns 0 if invalid, 1 if valid
 */

int check_args(int v, float f,  float a) {

  //Checks v is 0, 1, or 2
  if ((v < 0) || (v > 2)) {
    fatal_error("Invalid voice arg");
    return 0;
  }

  //Checks frequency is positive
  if (f < 0.0) {
    fatal_error("Invalid frequency arg");
  }

  //Checks amplitude is between 0.0 and 1.0
  if ((a < 0.0) || (a > 1.0)) {
    fatal_error("Invalid amplitude arg");
    return 0;
  }
  
  return 1;
}



int main (int argc, char** argv){
  if (argc != 6){
    printf("Error: expecting more arguments\n");
    return -1;
  }

  //First argument: voice
  int voice = atof(argv[1]);

  
  //Second argument: frequency
  float frequency = atof(argv[2]);


  //Third argument: amplitude
  float amplitude = atof(argv[3]);


  //Fourth argument: numsample
  int numsamples = atof(argv[4]); 

  //Fifth argument: output file name
  char* filename = argv[5];

  //printf("Voice: %d Freq: %f Amp: %f Samples: %d file: %s\n", voice, frequency, amplitude, //numsamples, filename); 

  int16_t *buffer = malloc(sizeof(short) * numsamples * 2);
  memset(buffer, 0, numsamples*2);

  if (check_args(voice, frequency, amplitude) != 0) {  


    //Sine wave
    if (voice == SINE) {
      render_sine_wave_stereo(buffer, numsamples, frequency, amplitude);
    }
    
    //If square wave
    else if (voice == SQUARE){
      render_square_wave_stereo(buffer, numsamples, frequency, amplitude);
    }

    //Sawtooth
    else if (voice == SAW) {
      render_saw_wave_stereo(buffer, numsamples, frequency, amplitude);
    }

    
    
    FILE *outputfile = fopen(filename, "wb+");
    if (outputfile == NULL){
      printf("Error: file failed to open");
      return 1;
    }

    write_wave_header(outputfile, numsamples);

    write_s16_buf(outputfile, buffer, numsamples*2);

    /*
    //Test print values of array
    for (int i=0; i<numsamples*2; i++){
      printf("Value i %d %d\n", i, buffer[i]);
    }

    fclose(outputfile);

    printf("Finished\n");
    */
    
     free(buffer);

     //Test to see if the computer uses little endian or big endian
     /*int test = 1;
    char *p = (char*)&test;

    if (p[0]==0){
      printf("Big endian\n");
    } else {
      printf("Little endian\n");
      }*/

     printf("Finished\n");

  }
  else{printf("Failed");}

  return 0;
}
