#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "io.h"


/*
 * Prints error message to stderr
 */
void fatal_error(const char* message) {

  fprintf(stderr, "Error: %s\n", message);
  
}

void write_byte(FILE* out, char val){

  fwrite(&val, sizeof(char), 1, out);
}

void write_bytes(FILE* out, const char data[], unsigned n){

  for (unsigned i=0; i<n; i++){
    write_byte(out, data[i]);
  }
}

void write_u16(FILE* out, uint16_t value){

  
  char bytes[] = {0, 0};
  //Lowest byte
  bytes[0] = value & 0xFF;
  //Biggest byte
  bytes[1] = value>>8;
  write_bytes(out, bytes, 2u);
}

void write_u32(FILE* out, uint32_t value){

  uint16_t first = value & 0x0000FFFF;
  uint16_t second = value >> 16;

  write_u16(out, first);
  write_u16(out, second);
  
    //fwrite(&value, sizeof(uint32_t), 1, out);
}

void write_s16(FILE* out, int16_t  value){

  
  
  uint16_t newV = (uint16_t)value;

  
  write_u16(out, newV);
}

void write_s16_buf(FILE* out, const int16_t  buf[], unsigned n){

  
  for (unsigned i=0; i<n; i++){
  
    write_s16(out, buf[i]);
  }
}


// Read

void read_byte(FILE* in, char *val){

  char value = fgetc(in);
  /*
  if (feof(in)){
    fatal_error("Could not read byte");
    }*/
  *val = value;
}

void read_bytes(FILE* in, char data[], unsigned n){

  for (unsigned i=0; i<n; i++){
    read_byte(in, &data[i]);
  }
}

void read_u16(FILE* in, uint16_t *value){


  
  char first_byte;
  read_byte(in, &first_byte);
  char second_byte;
  read_byte(in, &second_byte);
  uint16_t second = second_byte << 8;

  

  *value = second | first_byte;
}

void read_u32(FILE* in, uint32_t *value){
  
  uint16_t low, high;
  read_u16(in, &low);
  read_u16(in, &high);
  uint32_t second = high << 16;

  *value = second | low;
}

void read_s16(FILE* in, int16_t *value){

  
  char first_byte;
  read_byte(in, &first_byte);
  char second_byte;
  read_byte(in, &second_byte);
  uint16_t second = second_byte;

  uint8_t first = first_byte;

  //Unused, was giving compiler error.
  //uint16_t val = (second << 8) | first;

  int16_t val2 = (int16_t)((second<<8) | first);

  // printf("First %d Second %d Uint %u Int %d\n", first, second_byte, val, val2);
  
  *value = val2;

}

void read_s16_buf(FILE* in, int16_t  buf[], unsigned n){

  for (unsigned i=0; i<n; i++){
    read_s16(in, &buf[i]);
  }
}


