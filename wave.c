#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "io.h"
#include "wave.h"

//Wave

/*
 * Write a WAVE file header to given output stream.
 * Format is hard-coded as 44.1 KHz sample rate, 16 bit
 * signed samples, two channels.
 *
 * Parameters:
 *   out - the output stream
 *   num_samples - the number of (stereo) samples that will follow
 */
void write_wave_header(FILE *out, unsigned num_samples) {
  /*
   * See: http://soundfile.sapp.org/doc/WaveFormat/
   */

  uint32_t ChunkSize, Subchunk1Size, Subchunk2Size;
  uint16_t NumChannels = NUM_CHANNELS;
  uint32_t ByteRate = SAMPLES_PER_SECOND * NumChannels * (BITS_PER_SAMPLE/8u);
  uint16_t BlockAlign = NumChannels * (BITS_PER_SAMPLE/8u);

  /* Subchunk2Size is the total amount of sample data */
  Subchunk2Size = num_samples * NumChannels * (BITS_PER_SAMPLE/8u);
  Subchunk1Size = 16u;
  ChunkSize = 4u + (8u + Subchunk1Size) + (8u + Subchunk2Size);

  /* Write the RIFF chunk descriptor */
  write_bytes(out, "RIFF", 4u);
  write_u32(out, ChunkSize);
  write_bytes(out, "WAVE", 4u);

  /* Write the "fmt " sub-chunk */
  write_bytes(out, "fmt ", 4u);       /* Subchunk1ID */
  write_u32(out, Subchunk1Size);
  write_u16(out, 1u);                 /* PCM format */
  write_u16(out, NumChannels);
  write_u32(out, SAMPLES_PER_SECOND); /* SampleRate */
  write_u32(out, ByteRate);
  write_u16(out, BlockAlign);
  write_u16(out, BITS_PER_SAMPLE);

  /* Write the beginning of the "data" sub-chunk, but not the actual data */
  write_bytes(out, "data", 4);        /* Subchunk2ID */
  write_u32(out, Subchunk2Size);
}

/*
 * Read a WAVE header from given input stream.
 * Calls fatal_error if data can't be read, if the data
 * doesn't follow the WAVE format, or if the audio
 * parameters of the input WAVE aren't 44.1 KHz, 16 bit
 * signed samples, and two channels.
 *
 * Parameters:
 *   in - the input stream
 *   num_samples - pointer to an unsigned variable where the
 *      number of (stereo) samples following the header
 *      should be stored
 */
void read_wave_header(FILE *in, unsigned *num_samples) {
  char label_buf[4];
  uint32_t ChunkSize, Subchunk1Size, SampleRate, ByteRate, Subchunk2Size;
  uint16_t AudioFormat, NumChannels, BlockAlign, BitsPerSample;

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "RIFF", 4u) != 0) {
    fatal_error("Bad wave header (no RIFF label)");
  }

  read_u32(in, &ChunkSize); /* ignore */

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "WAVE", 4u) != 0) {
    fatal_error("Bad wave header (no WAVE label)");
  }

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "fmt ", 4u) != 0) {
    fatal_error("Bad wave header (no 'fmt ' subchunk ID)");
  }

  read_u32(in, &Subchunk1Size);
  if (Subchunk1Size != 16u) {
    fatal_error("Bad wave header (Subchunk1Size was not 16)");
  }

  read_u16(in, &AudioFormat);
  if (AudioFormat != 1u) {
    fatal_error("Bad wave header (AudioFormat is not PCM)");
  }

  read_u16(in, &NumChannels);
  if (NumChannels != NUM_CHANNELS) {
    fatal_error("Bad wave header (NumChannels is not 2)");
  }

  read_u32(in, &SampleRate);
  if (SampleRate != SAMPLES_PER_SECOND) {
    fatal_error("Bad wave header (Unexpected sample rate)");
  }

  read_u32(in, &ByteRate); /* ignore */

  read_u16(in, &BlockAlign); /* ignore */

  read_u16(in, &BitsPerSample);
  if (BitsPerSample != BITS_PER_SAMPLE) {
    fatal_error("Bad wave header (Unexpected bits per sample)");
  }

  read_bytes(in, label_buf, 4u);
  if (memcmp(label_buf, "data", 4u) != 0) {
    fatal_error("Bad wave header (no 'data' subchunk ID)");
  }

  /* finally we're at the Subchunk2Size field, from which we can
   * determine the number of samples */
  read_u32(in, &Subchunk2Size);
  *num_samples = Subchunk2Size / NUM_CHANNELS / (BITS_PER_SAMPLE/8u);
}

/* TODO: add additional functions... */

void render_square_wave_stereo(int16_t buf[], unsigned num_samples, float freq_hz, float amplitude){
  render_square_wave(buf, num_samples, 0, freq_hz, amplitude);
  render_square_wave(buf, num_samples, 1, freq_hz, amplitude);
}

void render_square_wave(int16_t buf[], unsigned num_samples, unsigned channel, float freq_hz, float amplitude){

  //0 is left channel, even indexes
  //1 is right channel, odd indexes
  

  //Length of whole sound
  float wlength = (float)SAMPLES_PER_SECOND / num_samples;
  //Length of one cycle
  float c_time = 1/freq_hz;
  //Number of cycles
  float num_cycles = wlength/c_time;
  //Samples per cycle & half cycle
  float fsamples_per_cycle = num_samples/num_cycles;
  int samples_per_cycle = (int)floor(fsamples_per_cycle);
  int half_cycle = samples_per_cycle/2;


  //Absolute index to whole sound
  int array_index = channel;

  

  //Index relative to the wave cycle
  int real_index = 0;
  for (unsigned i =0; i<num_samples; i++){

    //Positive 1 if first half of wave cycle
    //  -1 if second half
    int multiplier = 1;
    
    if (real_index > half_cycle){
      multiplier = -1;
    }

    int16_t value = INT16_MAX * multiplier * amplitude;
    
    //test
    //printf("%d ", value);
     
    buf[array_index] = value;

    array_index += 2;
      
    real_index++;
    if (real_index == samples_per_cycle){
      real_index = 0;
    }
  }


    //Updated square wave
    /*
  for (unsigned i=0; i<num_samples; i++){

    float time = i/SAMPLES_PER_SECOND;
    float sign = sin(freq_hz * time);
    int sign2 = 0;
    if (sign > 0){
      sign2=1;
    } else if (sign < 0){
      sign2=-1;
    }

    buf[array_index] = (int16_t)(amplitude*sign2);

    array_index += 2;
}
      

    */
}



//Waiting until square works to start w/ this


void render_sine_wave_stereo(int16_t buf[], unsigned num_samples, float freq_hz, float amplitude){
  render_sine_wave(buf, num_samples, 0, freq_hz, amplitude);
  render_sine_wave(buf, num_samples, 1, freq_hz, amplitude);
}

void render_sine_wave(int16_t buf[], unsigned num_samples, unsigned channel, float freq_hz, float amplitude){

  //0 is left channel, even indexes
  //1 is right channel, odd indexes

  
  //Length of whole sound
  float wlength = (float)SAMPLES_PER_SECOND / num_samples;
  //Length of one cycle
  float c_time = 1/freq_hz;
  //Number of cycles
  float num_cycles = wlength/c_time;
  //Samples per cycle
  float fsamples_per_cycle = num_samples/num_cycles;
  int samples_per_cycle = (int)floor(fsamples_per_cycle);


  
  //Absolute index for whole file
  //Staggered start for stereo audio
  int array_index = channel;
  
  //Index relative to the wave cycle
  int real_index = 0;

  for (unsigned i =0; i<num_samples; i++){

    /*Real index keeps track based on the wave-cycle,
    * 1/f_p_c is the decimal unit of time based on how many samples per cycle
    * This gives us the time, the decimal of where we are on the sin wave
    */
    float time = real_index * (1/fsamples_per_cycle);

    //Sin value
    float sin_val = sin(time * 2.0 * PI);

    //printf("%f ", sin_val);

    
    //Scale up the sin_val to proper size  
    int16_t value = amplitude * INT16_MAX * sin_val ;
    //printf("%d ", value);
     
    buf[array_index] = value;

    array_index += 2;

    //Reset at end of wave cycle
    real_index++;
    if (real_index == samples_per_cycle){
      real_index = 0;
    }
  }  
}

void render_saw_wave_stereo(int16_t buf[], unsigned num_samples, float freq_hz, float amplitude){
  render_saw_wave(buf, num_samples, 0, freq_hz, amplitude);
  render_saw_wave(buf, num_samples, 1, freq_hz, amplitude);
}

void render_saw_wave(int16_t buf[], unsigned num_samples, unsigned channel, float freq_hz, float amplitude){

  //0 is left channel, even indexes
  //1 is right channel, odd indexes
  
  //Length of whole sound
  float wlength = (float)SAMPLES_PER_SECOND / num_samples;
  //Length of one cycle
  float c_time = 1/freq_hz;
  //Number of cycles
  float num_cycles = wlength/c_time;
  //Samples per cycle
  float fsamples_per_cycle = num_samples/num_cycles;
  int samples_per_cycle = (int)floor(fsamples_per_cycle);


  //Absolute index for whole file
  //Staggered start for stereo audio
  int array_index = channel;
  //Index relative to the wave cycle
  int real_index = 0;
  //Bottom of saw
  float min_val = -1 * amplitude * INT16_MAX;
  //Top point of saw at end of wave cycle
  float max_val = amplitude * INT16_MAX;
  //Slope
  float inc = (max_val - min_val)/fsamples_per_cycle;
 
  for (unsigned i =0; i<num_samples; i++){
       
    //y = b + mx
    int16_t value = min_val + (real_index * inc);
     
    buf[array_index] = value;

    array_index += 2;

    //Reset at end of wave cycle
    real_index++;
    if (real_index == samples_per_cycle){
      real_index = 0;
    }
  }  
}





