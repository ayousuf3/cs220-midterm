#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "wave.h"
#include "io.h"

//Init function
float midi_to_freq(int midi);


/* Checks user file names.
 *
 * Params:: argc: Number of args.
 *          in: char pointer to input filename.
 *          out: char pointer to output filename.  
 *
 * Returns:: -1 if file types are bad.
 *            0 if file types are accepted.
 */
int check_files(int argc, char* in, char* out) {

  if (argc != 3) {
    fatal_error("Expecting 2 args");
    return -1;
  }

  int valid = 1;
  char* txt = ".txt";
  char* wav = ".wav";
  
  char* in_ext = malloc(sizeof(char) * 4);
  char* out_ext = malloc(sizeof(char) * 4);
  
  strncpy(in_ext, in+strlen(in)-4, strlen(in));
  strncpy(out_ext, out+strlen(out)-4, strlen(out));

  if (strcmp(in_ext, txt) != 0 ) {
    fatal_error("Must read from .txt file");
    valid = 0;
  }

  else if (strcmp(out_ext, wav) != 0) {
    fatal_error("Must write to .wav file");
    valid = 0;
  }

  free(in_ext);
  free(out_ext);
  
  if (valid == 0) {
    return -1;
  }

  else {
    return 0;
  }

  
  
}

/* Checks voice, frequency, and amplitude before writing note.
 *
 */
int check_args(int v, float f,  float a, int note_length, unsigned num_samples) {

  //Checks v is 0, 1, or 2
  if ((v < 0) || (v > 2)) {
    fatal_error("Invalid voice arg");
    return -1;
  }

  //Checks frequency is positive
  if (f < 0.0) {
    fatal_error("Invalid frequency arg");
    return -1;
  }

  //Checks amplitude is between 0.0 and 1.0
  if ((a < 0.0) || (a > 1.0)) {
    fatal_error("Invalid amplitude arg");
    return -1;
  }

  if ((note_length < 0)||(note_length > (int)num_samples)) {
    fatal_error("Invalid Note Size");
    return -1;
  }
  
  return 0;
}

/* Read song data from file.
 *
 * Params:: ptr: File ptr to .txt file w/ song data.
 *          directives: Array of strings of directives / song commands.
 *          num_samples_ptr: Unsigned int ptr, assigns number of samples / length of song.
 *          beat_length_ptr: Unsigned int ptr, assigns number of samples per beat.
 *
 * Returns:: num_directives: Number of directives in the song .txt file.
 */

int read_data(FILE* ptr, char **directives, unsigned* num_samples_ptr, unsigned* beat_length_ptr) {

  int num_directives = 0;
  char str[100];
  int i = 0;
  
  fscanf(ptr, " %u %u", num_samples_ptr, beat_length_ptr);
    
  while (fgets(str, 100, ptr) != NULL) {

      strcpy(directives[i], str);

      if (str[0] != '\0') {
	++num_directives;	
      }
      ++i;
  }
  return (num_directives);  
}


/* Writes note from either Note or Chord directive using render sine/saw/square wave functions.
 * 
 * Params:: buffer: int16_t array where actual song is written.
 *          position: unsigned int representing position in song.
 *          note_length: unsigned int representation length of note as number of samples.
 *          freq: float frequency value to write.
 *          amp: float amplitude to write.
 *          voice: int voice value to use when writing song.
 *
 * Return:: position: The current sample position in the song after the note has been written.
 */
int write_note(int16_t *buffer, unsigned position, unsigned note_length, float freq, float amp, int voice) {
  
  switch(voice) {

  case SINE:
    
    render_sine_wave_stereo(&buffer[position], note_length, freq, amp);
    break;
  case SQUARE:

    render_square_wave_stereo(&buffer[position], note_length, freq, amp);
    break;
  case SAW:

    render_saw_wave_stereo(&buffer[position], note_length, freq, amp);
    break;
    
  default:
    break;
    

  }
  position += note_length;
  return position;
}

/* Writes notes from Chord directive by sending them 1 by 1 to write_note.
 *
 * Params:: buffer: int16_t array where actual song is written.
 *          position: unsigned int representing position in song.
 *          note_length: unsigned int representation length of note as number of samples.
 *          notes: Array of unsigned ints that holds the notes
 *          num_notes: Number of notes in Notes array.
 *          freq: float frequency value to write.
 *          amp: float amplitude to write.
 *          voice: int voice value to use when writing song.
 *
 * Return:: position: The current sample position in the song after the chord is wirtten.
 *          -1: If note is invalid.
 */
int write_notes(int16_t *buffer, unsigned num_samples, unsigned position, int note_length, unsigned *notes, unsigned num_notes, float amp, int voice){

  float freq;
  
  for (int i = 0; i < (int)num_notes; ++i) {
    freq = midi_to_freq(notes[i]);


    if (check_args(voice, freq, amp, note_length, num_samples) != -1) {
      position = write_note(buffer, position, note_length, freq, amp, voice);
    } else {
      return -1;
    }
    
    
  }
  return position;  
}

/* Reads directives and writes appropriate song.
 *
 * Params:: buffer: int16_t array where actual song is written.
 *          directives: Array of strings holding the directives.
 *          num_samples: Unsigned int, holds number of samples / length of song.
 *          beat_length: Unsigned int, holds length of beat as number of samples.
 *          num_directives: Unsigned int, holds number of directives.
 *
 * Return:: -1: Writing song failed.
 *           0: Writing song successful.
 *
 */

int  write_song(int16_t *buffer, char **directives, unsigned num_samples, unsigned beat_length, unsigned num_directives) {

  unsigned position = 0;

  //Stores command of directive, first letter
  char command;

  //Note for N command
  int note;
  float note_beat;
  unsigned note_length;
 
  //Pause ebat for P command
  float pause;

  //Beat, notes, number of notes for C command
  // float chord_beat;
  unsigned  num_notes;
  unsigned notes[100];
  
  //Default amplitude
  float amp = 0.1;

  //Default voice
  int voice = SINE;

  //Note Frequency
  float freq;

  for (int i = 0; i < (int)num_directives; ++i) {
    
    sscanf(directives[i], "%c ", &command);

    switch(command) {
    case 'N':
      
      printf("Note \n");
      sscanf(directives[i], "%c %f %d", &command, &note_beat, &note);


      freq = midi_to_freq(note);

      
      note_length = beat_length * note_beat;

      if (check_args(voice, freq, amp, note_length, num_samples) == -1) {
	return -1;
      }
      
      
      position = write_note(buffer, position, note_length, freq, amp, voice);
      break;

    case 'P':
      printf("pause\n");
      sscanf(directives[i], "%c %f", &command, &pause);
      //printf("Pause %f \n", pause);
      break;
      
    case 'C':
      printf("Chord \n");

      sscanf(directives[i], "%c %f", &command, &note_beat);
      note_length = beat_length * note_beat;
      
      //Num notes based on length of string
      num_notes = (strlen(directives[i]) - 9) / 3;
      
      for (int j = 0; j < (int)num_notes; ++j) {
	char temp_note[100];
	strncpy(temp_note, (directives[i] + 6 + (3*j)), 2);
	notes[j] = atoi(temp_note);
      }

      position = write_notes(buffer, num_samples, position, note_length, notes, num_notes, amp, voice);

      if (position == 0) {
	return -1;
      }
      
      break;
      
    case 'V':
      sscanf(directives[i], "%c %d", &command, &voice);
      printf("Voice: %d \n", voice);
      break;
      
    case 'A':
      sscanf(directives[i], "%c %f", &command, &amp);
      printf("Amp: %f \n", amp);
    default:
      break;
    }

    
   
  }

  return 0;

}

/* Converts int representation of midi sound to frequency representation.
 *
 * Params:: midi: Int value of the midi note
 *
 * Return:: freq: Float freq representation of the midi note that our function can use.
 */
float midi_to_freq(int midi) {

  float exp = ((float)midi - 69.0) / 12.0;
  
  float freq = 440.0 * pow(2, exp);
  
  return freq;
}

/* Main
 *
 * Params:: argc, argv: Takes command line input.
 *
 * Retun: 0
 */

int main(int argc, char** argv) {

  //Get in/out file names.
  char* input_file = argv[1];
  char* output_file = argv[2];

  //If file names are valid.
  if (check_files(argc, input_file, output_file) != -1) {

    //File Pointers
    FILE* f_in_ptr = fopen(input_file, "r+");
    FILE* f_out_ptr = fopen(output_file, "wb+");

    //Number of samples in whole song
    //Length of beat as an int # of samles
    //Number of directives / commands
    unsigned num_samples;
    unsigned beat_length;
    unsigned num_directives;

    //Allocate array of strings for directives
    int max_directives = 1000;
    char **directives = malloc(max_directives * sizeof(char*));
    int max_directive_length = 100;    
    for (int i = 0; i < max_directives; ++i) {
      directives[i] = malloc(max_directive_length * sizeof(char));
    }

    //Read Data from file
    num_directives  = read_data(f_in_ptr, directives, &num_samples, &beat_length);

    //Allocate buffer for song, init values to 0.
    int16_t *buffer = malloc(sizeof(short) * num_samples * 2);
    memset(buffer, 0, num_samples * 2);

    //Write song data to buffer.
    int song_success;

    song_success = write_song(buffer, directives, num_samples, beat_length, num_directives);

    if (song_success == -1) {
      return 0;
    }
    
    //Write header for .wav file.
    write_wave_header(f_out_ptr, num_samples);

    //Write buffer to .wav file.
    write_s16_buf(f_out_ptr, buffer, num_samples*2);

    //Close file ptrs
    fclose(f_in_ptr);
    fclose(f_out_ptr);

    //Free allocated memory
    free(buffer);
    for (int i = 0; i < max_directives; ++i) {
      free(directives[i]);
    }
    free(directives);
    
  }
  return 0;
}
